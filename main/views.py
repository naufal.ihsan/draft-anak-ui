from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .models import Draft

response = {}

def index(request):
    response['author'] = "Naufal Ihsan Pratama"
    response['drafts'] = Draft.objects.all()
    html = 'main_app/main_app.html'
    return render(request, html, response)

def add_draft(request):
    if request.method == 'POST':
        sender   = request.POST['sender']
        reciever = request.POST['reciever']
        message  = request.POST['message']
        draft = Draft(sender=sender,reciever=reciever,message=message)
        draft.save()
        return HttpResponseRedirect('/home/')
