from django.conf.urls import url
from .views import index,add_draft

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add/$', add_draft, name='add_draft'),
]
