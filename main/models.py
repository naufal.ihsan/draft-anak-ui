from django.db import models

class Draft(models.Model):

	sender = models.CharField(max_length=27)
	reciever = models.CharField(max_length=27)
	message = models.CharField(max_length=255)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		ordering = ['-created_at']
