from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_draft
from .models import Draft


class MainAppUnitTest(TestCase):
    def test_main_app_url_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_main_app_using_index_func(self):
        found = resolve('/home/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_draft(self):
        # Creating a new activity
        new_activity = Draft.objects.create(sender='ihsan', reciever='nashi',message='dibalik doang namanya')

        # Retrieving all available activity
        counting_all_available_todo = Draft.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_main_app_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/home/add/', {'sender': test, 'reciever': test,'message': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/home/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)