# Tugas Eksplorasi Ristek Fasilkom UI 2018

>Project by Me

[![pipeline status](https://gitlab.com/naufal.ihsan/draft-anak-ui/badges/master/pipeline.svg)](https://gitlab.com/naufal.ihsan/draft-anak-ui/commits/master)  [![coverage report](https://gitlab.com/naufal.ihsan/draft-anak-ui/badges/master/coverage.svg)](https://gitlab.com/naufal.ihsan/draft-anak-ui/commits/master)
  
  Pada project kali ini membuat aplikasi Dari dan Untuk.

##  Creator

  Naufal Ihsan Pratama



## Gitlab and HerokuApp

  Gitlab repository : [https://gitlab.com/naufal.ihsan/draft-anak-ui/](https://gitlab.com/naufal.ihsan/draft-anak-ui/) <br/>
  Heroku app        : [https://draftanakui.herokuapp.com/](https://draftanakui.herokuapp.com/)


## Developing

  ```bash
    $ git clone https://gitlab.com/naufal.ihsan/draft-anak-ui.git
    $ cd draft-anak-ui/
  ```

  [![forthebadge](http://forthebadge.com/badges/built-by-developers.svg)](http://forthebadge.com)
